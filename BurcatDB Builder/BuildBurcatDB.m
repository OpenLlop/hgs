function BuildBurcatDB()

fname = 'BurcatTable.dat'; % File where BurcatTable is found

[elements,props] = CompileList(fname); % Get elements and props list

IndexCell = CreateIndexCell(elements); % Create cell index for given elements

BurcatDB = CompileDB(elements,props); % Compile BurcatDB and save it as .mat file

% Saves BurcatDB as a .mat file, ready to use in hgs
save('BurcatDB.mat','BurcatDB','IndexCell');

end

