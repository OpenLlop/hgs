%***********************************************************************************************************
%* HGS 1.1 
%* By Arnau Miro, Pau Manent, Eva Astrid Jara
%* Supervised by Manel Soria and Ramon Carreras
%* LLOP, ETSEIAT UPC          
%***********************************************************************************************************
%
% For any issues with the code see the documentation manual
%
% Usage:
%           M=molarmass(Element)
%
% Input:
%   Element     Empirical formulation of the element as a string (eg. 'CO2')
% Output:
%   M           Molar mass in [g/mol]

function M=Molarmass(Element)

for k=1:length(Element)
    especie=Element;
    M=0;
    
    fin=length(especie);
    for i=1:fin
        %Busca els car?cters de cada element
        element=especie(i);
        mult=1;
        if (i<fin)
            element2=especie(i+1);
            if (double(element2)>=65 && double(element2)<=90)%El seguent ?s majuscula
                element=especie(i);
                mult=1;
            elseif(double(element2)>=48 && double(element2)<=57)%El seguent ?s un nombre
                element=especie(i);
                mult=element2-48;
                if (i+1<length(especie))
                    element3=especie(i+2);
                    if(double(element3)>=48 && double(element3)<=57)%El seguent ?s un nombre
                        mult=(element3-48)+10*mult;
                    end
                end
            elseif(double(element2)>=97 && double(element2)<=122)%El seguent ?s minuscula
                element=especie(i:i+1);
                mult=1;
                if (i<fin-1)
                    element3=especie(i+2);
                    if (double(element3)>48 && double(element3)<57)%El seguent ?s un nombre
                        mult=element3-48;
                        if (i+2<length(especie))
                            element4=especie(i+3);
                            if(double(element4)>=48 && double(element4)<=57)%El seguent ?s un nombre
                                mult=(element4-48)+10*mult;
                            end
                        end
                    else
                        mult=1;
                    end
                end
            end
        end
        if (double(element)==40)
            break
        end
        %possa pes a cada element  
        %element
        switch element
            case 'H'
                m=1.0094;
            case 'Li'
                m=6.941;
            case 'Na'
                m=22.989;
            case 'K'
                m=39.098;
            case 'Rb'
                m=85.867;
            case 'Cs'
                m=132.905;
            case 'Fr'
                m=223;
            case 'Be'
                m=9.012;
            case 'Mg'
                m=24.305;
            case 'Ca'
                m=40.78;
            case 'Sr'
                m=87.62;
            case 'Ba'
                m=137.327;
            case 'Ra'
                m=226;
            case 'Sc'
                m=44.956;
            case 'Y'
                m=88.906;
            case 'Ti'
                m=47.867;
            case 'Zr'
                m=91.224;
            case 'Hf'
                m=178.49;
            case 'V'
                m=50.9415;
            case 'Nb'
                m=92.906;
            case 'Ta'
                m=180.948;
            case 'Cr'
                m=51.996;
            case 'Mo'
                m=95.94;
            case 'W'
                m=183.84;
            case 'Mn'
                m=54.938;
            case 'Tc'
                m=98;
            case 'Re'
                m=186.207;
            case 'Fe'
                m=55.845;
            case 'Ru'
                m=101.07;
            case 'Os'
                m=190.23;
            case 'Co'
                m=58.933;
            case 'Rh'
                m=102.905;
            case 'Ir'
                m=192.217;
            case 'Ni'
                m=58.693;
            case 'Pd'
                m=106.42;
            case 'Pt'
                m=195.078;
            case 'Cu'
                m=63.546;
            case 'Ag'
                m=107.868;
            case 'Au'
                m=196.966;
            case 'Zn'
                m=65.409;
            case 'Cd'
                m=65.409;
            case 'Hg'
                m=200.59;
            case 'B'
                m=10.811;
            case 'Al'
                m=26.981;
            case 'Ga'
                m=69.723;
            case 'In'
                m=114.818;
            case 'Tl'
                m=204.383;
            case 'C'
                m=12.0107;
            case 'Si'
                m=28.0855;
            case 'Ge'
                m=72.64;
            case 'Sn'
                m=118.71;
            case 'Pb'
                m=207.2;
            case 'N'
                m=14.00674;
            case 'P'
                m=30.974;
            case 'As'
                m=74.921;
            case 'Sb'
                m=121.76;
            case 'Bi'
                m=208.98;
            case 'O'
                m=15.9994;
            case 'S'
                m=32.066;
            case 'Se'
                m=78.96;
            case 'Te'
                m=127.60;
            case 'Po'
                m=209;
            case 'F'
                m=18.998;
            case 'Cl'
                m=35.453;
            case 'Br'
                m=79.904;
            case 'I'
                m=126.90447;
            case 'At'
                m=210;
            case 'He'
                m=4.0026;
            case 'Ne'
                m=20.1797;
            case 'Ar'
                m=39.948;
            case 'Kr'
                m=83.798;
            case 'Xe'
                m=131.293;
            case 'Rn'
                m=222;
            otherwise
                m=0;
        end
        %suma el pes de cada element
        M=M+m*mult;
    end
end
end


