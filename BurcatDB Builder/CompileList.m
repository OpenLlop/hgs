%***********************************************************************************************************
%* HGS 1.1 
%* By Arnau Miro, Pau Manent, Eva Astrid Jara
%* Supervised by Manel Soria and Ramon Carreras
%* LLOP, ETSEIAT UPC          
%***********************************************************************************************************
%
%* HGS. Compile List: Elements and properties list from Burcat Table.
%
% For any issues with the code see the documentation manual
%
% For internal code usage.

function [elements, props] = CompileList(fname)

elements = {}; % Define elements as a cell array

fid = fopen(fname); % opening the file

% Handle error if file not found
if fid == -1
    error('ERROR: File importation failed. Format is not supported');
end

i=1;

while (feof(fid) == 0) % loop until end of file is found

elements{i,1} = fscanf(fid,'%s',1);     % get elemets as cell array
props(i,:) = fscanf(fid,'%f',[1 15]);   % get properties as matrix

i = i+1;

end

% Save the variables to a .mat file
save('ElementsList.mat','elements');
save('PropertiesList.mat','props');

fclose(fid);

end