%***********************************************************************************************************
%* HGS 1.1 
%* By Arnau Miro, Pau Manent, Eva Astrid Jara
%* Supervised by Manel Soria and Ramon Carreras
%* LLOP, ETSEIAT UPC          
%***********************************************************************************************************
%
%* HGS. Compile Database: Compiles the hgs database from the list of elements and the list of properties.
%
% For any issues with the code see the documentation manual
%
% For internal code usage.

function [BurcatDB] = CompileDB(elements, props)

% Checks if used supplied the variables
% Loads the list of elements in a variable named 'elements'
if (~exist('elements','var') || isempty('elements')), load ElementsList; end
% Loads the properties of each element in a variable named 'props'
if (~exist('props','var') || isempty('props')), load PropertiesList; end

elem = length(elements);
BurcatDB = cell(elem,1);
for i=1:elem
    
    BurcatDB{i}=struct('Element',elements(i),...        % List the element as char array
                       'M',Molarmass(elements{i}),...   % Compute its molar mass
                       'CP_L',props(i,8:14),...         % Save CP for low temperature
                       'CP_H',props(i,1:7));            % Save CP for high temperature
end 

end