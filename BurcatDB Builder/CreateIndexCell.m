function [index] = CreateIndexCell(elements)

% If no imput is provided load the .mat file
if nargin ~= 1
    load ElementsList.mat
end

% Loop to write all the elements inside the cell
lelem = length(elements);
index = cell(lelem,2);
for i=1:lelem
    index{i,1} = i;
    index{i,2} = elements{i};
end

end