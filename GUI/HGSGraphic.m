function HGSGraphic
%***********************************************************************************************************
%* HGS GRAPHIC v1.0 
%* By Lluis Amat Olondriz and Xavier Rodado Sanchez
%* With the collaboration of Manel Soria and Arnau Miro
%*
%* ETSEIAT UPC          
%***********************************************************************************************************

global BurcatDB %#ok

% Clear global workspace
clear global;

% Definition of variables
load BurcatDB

% Addpath
addpath(genpath('GUI'));
addpath(genpath('HelpFiles'));
addpath(genpath('Rocket Performance'));

% Creating of main window
MainWindow;