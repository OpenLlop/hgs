function Table = Burcat2Cell(BurcatDB)
%***********************************************************************************************************
%* HGS GRAPHIC v1.0 
%* By Arnau Miro
%*
%* ETSEIAT UPC          
%***********************************************************************************************************

% Get no of elements
len = length(BurcatDB);

% Preallocate
Table = cell(len,16);

% Loop for each element
for i = 1:len
    % Read element and store it
    Table{i,1} = BurcatDB{i}.Element;
    Table{i,2} = BurcatDB{i}.M;
    Table(i,3:9) = num2cell(BurcatDB{i}.CP_H);
    Table(i,10:16) = num2cell(BurcatDB{i}.CP_L);
end

end