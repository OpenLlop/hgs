function varargout = ElementsDatabase(varargin)
%***********************************************************************************************************
%* HGS GRAPHIC v1.0 
%* By Arnau Miro
%*
%* ETSEIAT UPC          
%***********************************************************************************************************
% ELEMENTSDATABASE MATLAB code for ElementsDatabase.fig
%      ELEMENTSDATABASE, by itself, creates a new ELEMENTSDATABASE or raises the existing
%      singleton*.
%
%      H = ELEMENTSDATABASE returns the handle to a new ELEMENTSDATABASE or the handle to
%      the existing singleton*.
%
%      ELEMENTSDATABASE('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in ELEMENTSDATABASE.M with the given input arguments.
%
%      ELEMENTSDATABASE('Property','Value',...) creates a new ELEMENTSDATABASE or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before ElementsDatabase_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to ElementsDatabase_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help ElementsDatabase

% Last Modified by GUIDE v2.5 13-Feb-2015 19:34:04

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @ElementsDatabase_OpeningFcn, ...
                   'gui_OutputFcn',  @ElementsDatabase_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before ElementsDatabase is made visible.
function ElementsDatabase_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to ElementsDatabase (see VARARGIN)

% Choose default command line output for ElementsDatabase
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes ElementsDatabase wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = ElementsDatabase_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes during object creation, after setting all properties.
function elementtable_CreateFcn(hObject, eventdata, handles)
% hObject    handle to elementtable (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
global BurcatDB
if (~exist('BurcatDB','var') || isempty(BurcatDB)), load BurcatDB; end

set(hObject,'Data', Burcat2Cell(BurcatDB));

% --- Executes on button press in addmodify.
function addmodify_Callback(hObject, eventdata, handles)
% hObject    handle to addmodify (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global BurcatDB
global IndexCell

% Get current database
Database = get(handles.elementtable,'Data');
NewElement = get(handles.elementedit,'Data');

% Get old path
OldPath = which('BurcatDB.mat');

% Find element index
index = find(ismember(Database(:,1),NewElement(1)), 1);

%Element is not member
if isempty(index), index = length(BurcatDB)+1; end

% Overwrite in Burcat DB
BurcatDB{index}.Element = NewElement{1};
BurcatDB{index}.M = NewElement{2};
BurcatDB{index}.CP_H = [NewElement{3:9}];
BurcatDB{index}.CP_L = [NewElement{10:16}];

% Overwrite in IndexCell
IndexCell{index,2} = NewElement{1};

% Save new BurcatDB
save('BurcatDB.mat','BurcatDB','IndexCell');
NewPath = which('BurcatDB.mat');

% Overwrite BurcatDB
movefile(NewPath,OldPath);

% Reload element table
load BurcatDB
set(handles.elementtable,'Data', Burcat2Cell(BurcatDB));

% --- Executes on button press in restoredb.
function restoredb_Callback(hObject, eventdata, handles)
% hObject    handle to restoredb (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global BurcatDB

% Restore backup to DB
DBPath = which('BurcatDB.mat');
bakPath = which('BurcatDB_bak.mat');

copyfile(bakPath,DBPath);

% Reload element table
load BurcatDB
set(handles.elementtable,'Data', Burcat2Cell(BurcatDB));

% --- Executes during object creation, after setting all properties.
function elementedit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to elementedit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
data={' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' '};
set(hObject,'Data',data);


function SpeciesSearcher_Callback(hObject, eventdata, handles)
% hObject    handle to SpeciesSearcher (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get burcatDB cell to search
Database = get(handles.elementtable,'Data');
Elements = Database(:,1);

% gets the element introduced by the user
Search_term=get(hObject,'String');

% Find element index
index = find(ismember(Elements,Search_term));

% Load element in second table
if ~isempty(index)
    set(handles.elementedit,'Data',Database(index,:));
else
    warndlg(sprintf('Element %s not found.',Search_term{:}),'Element not found');
end


% --- Executes during object creation, after setting all properties.
function SpeciesSearcher_CreateFcn(hObject, eventdata, handles)
% hObject    handle to SpeciesSearcher (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
