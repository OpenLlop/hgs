function ChangeFigureIcon(hObject,icon, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject handle to figure
% icon name
% eventdata reserved - to be defined in a future version of MATLAB
% handles structure with handles and user data (see GUIDATA)
% varargin command line arguments to main (see VARARGIN)
 
% Choose default command line output for main
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);
 
javaFrame = get(hObject,'JavaFrame');
javaFrame.setFigureIcon(javax.swing.ImageIcon(icon));
end