function varargout = MainWindow(varargin)
%***********************************************************************************************************
%* HGS GRAPHIC v1.0 
%* By Lluis Amat Olondriz and Xavier Rodado Sanchez
%* With the collaboration of Manel Soria and Arnau Miro
%*
%* ETSEIAT UPC          
%***********************************************************************************************************
% MAINWINDOW MATLAB code for MainWindow.fig
%      MAINWINDOW, by itself, creates a new MAINWINDOW or raises the existing
%      singleton*.
%
%      H = MAINWINDOW returns the handle to a new MAINWINDOW or the handle to
%      the existing singleton*.
%
%      MAINWINDOW('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in MAINWINDOW.M with the given input arguments.
%
%      MAINWINDOW('Property','Value',...) creates a new MAINWINDOW or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before MainWindow_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to MainWindow_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help MainWindow

% Last Modified by GUIDE v2.5 16-Feb-2015 19:16:13

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @MainWindow_OpeningFcn, ...
                   'gui_OutputFcn',  @MainWindow_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before MainWindow is made visible.
function MainWindow_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to MainWindow (see VARARGIN)

% Choose default command line output for MainWindow
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);


% UIWAIT makes MainWindow wait for user response (see UIRESUME)
% uiwait(handles.hgsgui);


% --- Outputs from this function are returned to the command line.
function varargout = MainWindow_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --------------------------------------------------------------------
function file_Callback(hObject, eventdata, handles)
% hObject    handle to file (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function view_Callback(hObject, eventdata, handles)
% hObject    handle to view (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function help_Callback(hObject, eventdata, handles)
% hObject    handle to help (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function new_Callback(hObject, eventdata, handles)
% hObject    handle to new (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

CleanHGS(handles);

% --------------------------------------------------------------------
function openfile_Callback(hObject, eventdata, handles)
% hObject    handle to openfile (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get output dir
 [FileName,PathName] = uigetfile('*.hgs');
 file = fullfile(PathName,FileName);
 
 % Save data to file
LoadHGS(file,handles);


% --------------------------------------------------------------------
function savefile_Callback(hObject, eventdata, handles)
% hObject    handle to savefile (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get output dir
PathName = pwd;
FileName = 'case.hgs';
file = fullfile(PathName,FileName);

% Save data to file
SaveHGS(file,FileName,handles);


% --------------------------------------------------------------------
function saveas_Callback(hObject, eventdata, handles)
% hObject    handle to saveas (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get output dir
 [FileName,PathName] = uiputfile('*.hgs');
 file = fullfile(PathName,FileName);
 
 % Save data to file
SaveHGS(file,FileName,handles);


% --------------------------------------------------------------------
function exit_Callback(hObject, eventdata, handles)
% hObject    handle to exit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
delete(hObject);


% --- Executes when user attempts to close hgsgui.
function hgsgui_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to hgsgui (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: delete(hObject) closes the figure
delete(gcf);



function champressure3_Callback(hObject, eventdata, handles)
% hObject    handle to champressure3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of champressure3 as text
%        str2double(get(hObject,'String')) returns contents of champressure3 as a double


% --- Executes during object creation, after setting all properties.
function champressure3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to champressure3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function propellant_Callback(hObject, eventdata, handles)
% hObject    handle to propellant (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of propellant as text
%        str2double(get(hObject,'String')) returns contents of propellant as a double


% --- Executes during object creation, after setting all properties.
function propellant_CreateFcn(hObject, eventdata, handles)
% hObject    handle to propellant (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function mixtratio_Callback(hObject, eventdata, handles)
% hObject    handle to mixtratio (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of mixtratio as text
%        str2double(get(hObject,'String')) returns contents of mixtratio as a double


% --- Executes during object creation, after setting all properties.
function mixtratio_CreateFcn(hObject, eventdata, handles)
% hObject    handle to mixtratio (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in popupmenu1.
function popupmenu1_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu1


% --- Executes during object creation, after setting all properties.
function popupmenu1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in pressureunits3.
function pressureunits3_Callback(hObject, eventdata, handles)
% hObject    handle to pressureunits3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns pressureunits3 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from pressureunits3


% --- Executes during object creation, after setting all properties.
function pressureunits3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to pressureunits3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --------------------------------------------------------------------
function equilibrium_OnCallback(hObject, eventdata, handles)
% hObject    handle to equilibriumpanel (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.toolbar_menu.Children(4),'state','on');
set(handles.toolbar_menu.Children(3),'state','off');
set(handles.toolbar_menu.Children(2),'state','off');
set(handles.equilibriumpanel,'visible','on');
set(handles.isenthalpicpanel,'visible','off');
set(handles.isentropicpanel,'visible','off');
set(handles.title,'String','HGS. Equilibrium');

% --------------------------------------------------------------------
function isenthalpic_OnCallback(hObject, eventdata, handles)
% hObject    handle to isenthalpic (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.toolbar_menu.Children(4),'state','off');
set(handles.toolbar_menu.Children(3),'state','on');
set(handles.toolbar_menu.Children(2),'state','off');
set(handles.equilibriumpanel,'visible','off');
set(handles.isenthalpicpanel,'visible','on');
set(handles.isentropicpanel,'visible','off');
set(handles.title,'String','HGS. Adiabatic Flame Temperature');

% --------------------------------------------------------------------
function isentropic_OnCallback(hObject, eventdata, handles)
% hObject    handle to isentropic (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.toolbar_menu.Children(4),'state','off');
set(handles.toolbar_menu.Children(3),'state','off');
set(handles.toolbar_menu.Children(2),'state','on');
set(handles.equilibriumpanel,'visible','off');
set(handles.isenthalpicpanel,'visible','off');
set(handles.isentropicpanel,'visible','on');
set(handles.title,'String','HGS. Isentropic Expansion');


% --- Executes during object creation, after setting all properties.
function propellantmenu_CreateFcn(hObject, eventdata, handles)
% hObject    handle to propellantmenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
%load ElementsList
%set(hObject,'String',elements);
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in propellantmenu.
function propellantmenu_Callback(hObject, eventdata, handles)
% hObject    handle to propellantmenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns propellantmenu contents as cell array
%        contents{get(hObject,'Value')} returns selected item from propellantmenu


% --- Executes on selection change in reactionelements.
function reactionelements_Callback(hObject, eventdata, handles)
% hObject    handle to reactionelements (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns reactionelements contents as cell array
%        contents{get(hObject,'Value')} returns selected item from reactionelements


% --- Executes during object creation, after setting all properties.
function reactionelements_CreateFcn(hObject, eventdata, handles)
% hObject    handle to reactionelements (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
%load ElementsList
%set(hObject,'String',elements);
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --------------------------------------------------------------------
function elementdb_Callback(hObject, eventdata, handles)
% hObject    handle to elementdb (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
ElementsDatabase



function nozzlepressure_Callback(hObject, eventdata, handles)
% hObject    handle to nozzlepressure (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of nozzlepressure as text
%        str2double(get(hObject,'String')) returns contents of nozzlepressure as a double


% --- Executes during object creation, after setting all properties.
function nozzlepressure_CreateFcn(hObject, eventdata, handles)
% hObject    handle to nozzlepressure (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in nozzlepressureunits.
function nozzlepressureunits_Callback(hObject, eventdata, handles)
% hObject    handle to nozzlepressureunits (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns nozzlepressureunits contents as cell array
%        contents{get(hObject,'Value')} returns selected item from nozzlepressureunits

% --- Executes during object creation, after setting all properties.
function popupmenu6_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% --- Executes during object creation, after setting all properties.
function title_CreateFcn(hObject, eventdata, handles)
% hObject    handle to title (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called


% --- Executes on selection change in mixtratiomenu.
function mixtratiomenu_Callback(hObject, eventdata, handles)
% hObject    handle to mixtratiomenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns mixtratiomenu contents as cell array
%        contents{get(hObject,'Value')} returns selected item from mixtratiomenu


% --- Executes during object creation, after setting all properties.
function mixtratiomenu_CreateFcn(hObject, eventdata, handles)
% hObject    handle to mixtratiomenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on key press with focus on condpressure and none of its controls.
function condpressure_KeyPressFcn(hObject, eventdata, handles)
% hObject    handle to condpressure (see GCBO)
% eventdata  structure with the following fields (see UICONTROL)
%	Key: name of the key that was pressed, in lower case
%	Character: character interpretation of the key(s) that was pressed
%	Modifier: name(s) of the modifier key(s) (i.e., control, shift) pressed
% handles    structure with handles and user data (see GUIDATA)



% --- Executes on key press with focus on condarea and none of its controls.
function condarea_KeyPressFcn(hObject, eventdata, handles)
% hObject    handle to condarea (see GCBO)
% eventdata  structure with the following fields (see UICONTROL)
%	Key: name of the key that was pressed, in lower case
%	Character: character interpretation of the key(s) that was pressed
%	Modifier: name(s) of the modifier key(s) (i.e., control, shift) pressed
% handles    structure with handles and user data (see GUIDATA)



% --- Executes on key press with focus on condpressureratio and none of its controls.
function condpressureratio_KeyPressFcn(hObject, eventdata, handles)
% hObject    handle to condpressureratio (see GCBO)
% eventdata  structure with the following fields (see UICONTROL)
%	Key: name of the key that was pressed, in lower case
%	Character: character interpretation of the key(s) that was pressed
%	Modifier: name(s) of the modifier key(s) (i.e., control, shift) pressed
% handles    structure with handles and user data (see GUIDATA)

% --------------------------------------------------------------------
function manual_Callback(hObject, eventdata, handles)
% hObject    handle to manual (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
open('help.pdf')

% --------------------------------------------------------------------
function runanalysis_ClickedCallback(hObject, eventdata, handles)
% hObject    handle to runanalysis (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%1: Get current case
ce = get(handles.equilibrium,'State');
af = get(handles.isenthalpic,'State');
is = get(handles.isentropic,'State');

% Run for chemical equilibrium
if strcmp(ce,'on')
    try % normal code under try
        % Get table data
        reactdata = get(handles.speciestable,'Data');
        proddata = get(handles.productstable,'Data');

        % Build species 
        species = { reactdata{:,1}, proddata{:,1} };

        % Build moles
        nr(1:length(species)) = 0;
        nr(1:length(reactdata(:,1))) = cellfun(@str2double,reactdata(:,2));

        % Get temperature
        Traw = cellfun(@str2double,reactdata(:,3));
        T = mean(Traw);

        % Get pressure
        P = str2double(get(handles.champressure,'String'));

        % Launch HGS
        np = hgseq(species,nr,T,P(1));

        % Write results in table
        TableData = cell(length(species),2);
        TableData(:,1) = species(:); TableData(:,2) = num2cell(np);
        set(handles.outputtable,'Data',TableData);
        
    catch err % Error capture
        % Build error string
        [~,file,~] = fileparts(err.stack(1).file);
        errstr = sprintf('Error in %s line %d: %s',file,err.stack(1).line,err.message);
        % Launch error dialog box
        beep; % force a beep
        errordlg(errstr,'HGS Error');
        % Do other stuff to release the GUI
    end
end

% Run for isenthalpic
if strcmp(af,'on')
    try % normal code under try
        % Get table data
        reactdata = get(handles.speciestable2,'Data');
        proddata = get(handles.productstable2,'Data');

        % Build species 
        species = { reactdata{:,1}, proddata{:,1} };

        % Build moles
        nr(1:length(species)) = 0;
        nr(1:length(reactdata(:,1))) = cellfun(@str2double,reactdata(:,2));

        % Get temperature
        Traw = cellfun(@str2double,reactdata(:,3));
        T = mean(Traw);

        % Get pressure
        P = str2double(get(handles.champressure2,'String'));

        % Launch HGS
        [Tp,np] = hgsTp(species,nr,T,P(1));

        % Write results in table
        TableData = cell(length(species),2);
        TableData(:,1) = species(:); TableData(:,2) = num2cell(np);
        set(handles.outputtable2,'Data',TableData);
        set(handles.equilTemp,'String',sprintf('%.4f',Tp));
        
    catch err % Error capture
        % Build error string
        [~,file,~] = fileparts(err.stack(1).file);
        errstr = sprintf('Error in %s line %d: %s',file,err.stack(1).line,err.message);
        % Launch error dialog box
        beep; % force a beep
        errordlg(errstr,'HGS Error');
        % Do other stuff to release the GUI
    end
end

% Run for isentropic
if strcmp(is,'on')
    try % normal code under try
        % Get table data
        reactdata = get(handles.speciestable3,'Data');

        % Build species 
        species = reactdata(:,1);

        % Build moles
        nr(1:length(species)) = 0;
        if isnumeric(reactdata{1,2})
             nr(1:length(reactdata(:,1))) = [reactdata{:,2}];
        else
            nr(1:length(reactdata(:,1))) = cellfun(@str2double,reactdata(:,2));
        end

        % Get temperature
        Traw = cellfun(@str2double,reactdata(:,3));
        T1 = mean(Traw);

        % Get pressure
        P1 = str2double(get(handles.champressure3,'String'));
        
        % Get flow type
        if get(handles.equil,'Value')
            eql = 'shifting';
        elseif get(handles.frozen,'Value')
            eql = 'frozen';
        end
        
        % Get outside pressure
        if get(handles.ambpressure,'Value')
            P2 = 1;
        elseif get(handles.custpressure,'Value')
            P2 = str2double( get(handles.outpressure,'String') );
        end

        % Launch HGS
        [ Tp,np,v,M ] = hgsisentropic( species,nr,T1,P1,P2,eql );
        [~,~,MM,R,gamma,~] = hgsprop(species,np,Tp,P2);
        [Isp,cstar] = RocketPerformance(v,gamma,R,MM,T1);

        % Write results in table
        TableData = cell(length(species),2);
        TableData(:,1) = species(:); TableData(:,2) = num2cell(np);
        set(handles.outputtable3,'Data',TableData);
        
        % Write results table
        ResultsTable = cell(9,2);
        
        ResultsTable(1,:) = {1,'bar'};
        ResultsTable(2,:) = {P2,'bar'};
        ResultsTable(3,:) = {gamma,' '};
        ResultsTable(4,:) = {Tp,'K'}; 
        ResultsTable(5,:) = {M,' '}; 
        ResultsTable(6,:) = {v,'m/s'};
        ResultsTable(7,:) = {Isp,'s'};
        ResultsTable(8,:) = {cstar,'m/s'};
        ResultsTable(9,:) = {MM,'g/mol'};
        
        set(handles.performancetable,'Data',ResultsTable);
        
    catch err % Error capture
        % Build error string
        [~,file,~] = fileparts(err.stack(1).file);
        errstr = sprintf('Error in %s line %d: %s',file,err.stack(1).line,err.message);
        % Launch error dialog box
        beep; % force a beep
        errordlg(errstr,'HGS Error');
        % Do other stuff to release the GUI
    end
end

function eps_Callback(hObject, eventdata, handles)
% hObject    handle to eps (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of eps as text
%        str2double(get(hObject,'String')) returns contents of eps as a double


% --- Executes during object creation, after setting all properties.
function eps_CreateFcn(hObject, eventdata, handles)
% hObject    handle to eps (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function outpressure_Callback(hObject, eventdata, handles)
% hObject    handle to outpressure (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of outpressure as text
%        str2double(get(hObject,'String')) returns contents of outpressure as a double


% --- Executes during object creation, after setting all properties.
function outpressure_CreateFcn(hObject, eventdata, handles)
% hObject    handle to outpressure (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in outunits.
function outunits_Callback(hObject, eventdata, handles)
% hObject    handle to outunits (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns outunits contents as cell array
%        contents{get(hObject,'Value')} returns selected item from outunits


% --- Executes during object creation, after setting all properties.
function outunits_CreateFcn(hObject, eventdata, handles)
% hObject    handle to outunits (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes during object creation, after setting all properties.
function uipanel7_CreateFcn(hObject, eventdata, handles)
% hObject    handle to uipanel7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called


% --- Executes when selected object is changed in uipanel7.
function uipanel7_SelectionChangeFcn(hObject, eventdata, handles)
% hObject    handle to the selected object in uipanel7 
% eventdata  structure with the following fields (see UIBUTTONGROUP)
%	EventName: string 'SelectionChanged' (read only)
%	OldValue: handle of the previously selected object or empty if none was selected
%	NewValue: handle of the currently selected object
% handles    structure with handles and user data (see GUIDATA)
switch get(eventdata.NewValue,'Tag') % Get Tag of selected object.
    case 'ambpressure'
        set(handles.outpressure,'Enable','off')
        set(handles.outunits,'Enable','off')
    case 'custpressure'
        set(handles.outpressure,'Enable','on')
        set(handles.outunits,'Enable','on')
end

function edit12_Callback(hObject, eventdata, handles)
% hObject    handle to champressure3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of champressure3 as text
%        str2double(get(hObject,'String')) returns contents of champressure3 as a double


% --- Executes during object creation, after setting all properties.
function edit12_CreateFcn(hObject, eventdata, handles)
% hObject    handle to champressure3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in pressureunits3.
function popupmenu10_Callback(hObject, eventdata, handles)
% hObject    handle to pressureunits3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns pressureunits3 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from pressureunits3


% --- Executes during object creation, after setting all properties.
function popupmenu10_CreateFcn(hObject, eventdata, handles)
% hObject    handle to pressureunits3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on button press in AddSpecies2.
function AddSpecies_Callback(hObject, eventdata, handles)
% hObject    handle to AddSpecies2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global TableSelect
TableSelect = 0; % Select reactives table

handles.interface_species = interface_species('iconEditor', hObject);

% --- Executes on button press in AddSpecies3.
function AddSpecies3_Callback(hObject, eventdata, handles)
% hObject    handle to AddSpecies3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of AddSpecies3
global TableSelect
TableSelect = 4; % Select reactives table

handles.interface_species = interface_species('iconEditor', hObject);

% --- Executes on button press in Addproduct2.
function Addproduct_Callback(hObject, eventdata, handles)
% hObject    handle to Addproduct2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global TableSelect
TableSelect = 1; % Select reactives table

handles.interface_species = interface_species('iconEditor', hObject);

% --- Executes on button press in AddSpecies2.
function AddSpecies2_Callback(hObject, eventdata, handles)
% hObject    handle to AddSpecies2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global TableSelect
TableSelect = 2; % Select reactives table

handles.interface_species = interface_species('iconEditor', hObject);

% --- Executes on button press in Addproduct2.2
function Addproduct2_Callback(hObject, eventdata, handles)
% hObject    handle to Addproduct2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global TableSelect
TableSelect = 3; % Select reactives table

handles.interface_species = interface_species('iconEditor', hObject);

function champressure_Callback(hObject, eventdata, handles)
% hObject    handle to champressure2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of champressure2 as text
%        str2double(get(hObject,'String')) returns contents of champressure2 as a double

function champressure2_Callback(hObject, eventdata, handles)
% hObject    handle to champressure2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of champressure2 as text
%        str2double(get(hObject,'String')) returns contents of champressure2 as a double

% --- Executes during object creation, after setting all properties.
function champressure_CreateFcn(hObject, eventdata, handles)
% hObject    handle to champressure2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes during object creation, after setting all properties.
function champressure2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to champressure2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in pressureunits2.
function pressureunits2_Callback(hObject, eventdata, handles)
% hObject    handle to pressureunits2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns pressureunits2 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from pressureunits2

% --- Executes during object creation, after setting all properties.
function pressureunits_CreateFcn(hObject, eventdata, handles)
% hObject    handle to pressureunits2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes during object creation, after setting all properties.
function pressureunits2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to pressureunits2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function equilTemp_Callback(hObject, eventdata, handles)
% hObject    handle to equilTemp (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of equilTemp as text
%        str2double(get(hObject,'String')) returns contents of equilTemp as a double


% --- Executes during object creation, after setting all properties.
function equilTemp_CreateFcn(hObject, eventdata, handles)
% hObject    handle to equilTemp (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on button press in export.
function export_Callback(hObject, eventdata, handles)
% hObject    handle to export (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get output table and move to speciestable3
out = get(handles.outputtable2,'Data');
TableData = cell(length(out),4);
TableData(:,1:2) = out;
TableData(:,3) = cellstr(get(handles.equilTemp,'String'));
TableData(:,4) = cellstr('K');

set(handles.speciestable3,'Data',TableData);

% Set pressure
set(handles.champressure3,'String',get(handles.champressure2,'String'));

% Move to isentropic
isentropic_OnCallback(hObject, eventdata, handles);


% --------------------------------------------------------------------
function aboutus_Callback(hObject, eventdata, handles)
% hObject    handle to aboutus (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
aboutus('iconEditor', hObject)


% --------------------------------------------------------------------
function new_ClickedCallback(hObject, eventdata, handles)
% hObject    handle to new (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
CleanHGS(handles);

% --------------------------------------------------------------------
function openfile_ClickedCallback(hObject, eventdata, handles)
% hObject    handle to openfile (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get output dir
 [FileName,PathName] = uigetfile('*.hgs');
 file = fullfile(PathName,FileName);
 
 % Save data to file
LoadHGS(file,handles);


% --- Executes on button press in exportisenthalpic.
function exportisenthalpic_Callback(hObject, eventdata, handles)
% hObject    handle to exportisenthalpic (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get output table and move to speciestable2
set(handles.speciestable2,'Data',get(handles.speciestable,'Data'));
set(handles.productstable2,'Data',get(handles.productstable,'Data'));

% Set pressure
set(handles.champressure2,'String',get(handles.champressure,'String'));

% Move to isentropic
isenthalpic_OnCallback(hObject, eventdata, handles);


% --------------------------------------------------------------------
function savefile_ClickedCallback(hObject, eventdata, handles)
% hObject    handle to savefile (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get output dir
PathName = pwd;
FileName = 'case.hgs';
file = fullfile(PathName,FileName);

% Save data to file
SaveHGS(file,FileName,handles);
