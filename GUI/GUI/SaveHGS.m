function SaveHGS(file,FileName,handles)
%***********************************************************************************************************
%* HGS GRAPHIC v1.0 
%* By Lluis Amat Olondriz and Xavier Rodado Sanchez
%* With the collaboration of Manel Soria and Arnau Miro
%*
%* ETSEIAT UPC          
%***********************************************************************************************************

% Open file for writing
if exist(FileName,'file')
     button = questdlg(sprintf('Data in %s will be overwritten. Are you sure?',FileName),'Overwrite');
end

% Save data to file
if ~exist(FileName,'file') || strcmp(button,'Yes')
    fid  = fopen(file,'w');
    % Write heading
    fprintf(fid,'# HGS Graphic save v1.0 with HGS v.1.3. Save log file\n\n');
    
    % Write current case
    if strcmp(get(handles.equilibrium,'State'),'on')
        SaveEquilibrium(fid,handles);
    end
    if strcmp(get(handles.isenthalpic,'State'),'on')
        SaveIsenthalpic(fid,handles);
    end
    if strcmp(get(handles.isentropic,'State'),'on')
        SaveIsentropic(fid,handles);
    end    
    % Close file2
    fclose(fid);
end

end

function SaveEquilibrium(fid,handles)

fprintf(fid,'equilibrium\t #Current running case\n\n');

% Write pressure
fprintf(fid,'%s; %d;\t # Reaction pressure\n\n',...
    get(handles.champressure,'String'),...
    get(handles.pressureunits,'Value'));

% Write reactives
reactdata = get(handles.speciestable,'Data');
fprintf(fid,'%d\t # Reactives\n',length(reactdata(:,1)));
for i = 1:length(reactdata(:,1))
    fprintf(fid,'%s;\t',reactdata{i,1});
end
fprintf(fid,'\n');

% Write number of mols
fprintf(fid,'# No of mols\n');
for i = 1:length(reactdata(:,2))
    fprintf(fid,'%s;\t',reactdata{i,2});
end
fprintf(fid,'\n');

% Write temperature
fprintf(fid,'# Temperature\n');
for i = 1:length(reactdata(:,3))
    fprintf(fid,'%s;\t',reactdata{i,3});
end
fprintf(fid,'\n\n');

% Write products
proddata = get(handles.productstable,'Data');
fprintf(fid,'%d\t # Products\n',length(proddata(:,1)));
for i = 1:length(proddata(:,1))
    fprintf(fid,'%s;\t',proddata{i,1});
end
fprintf(fid,'\n');

end

function SaveIsenthalpic(fid,handles)

fprintf(fid,'isenthalpic\t #Current running case\n\n');

% Write pressure
fprintf(fid,'%s; %d;\t # Reaction pressure\n\n',...
    get(handles.champressure2,'String'),...
    get(handles.pressureunits2,'Value'));

% Write reactives
reactdata = get(handles.speciestable2,'Data');
fprintf(fid,'%d\t # Reactives\n',length(reactdata(:,1)));
for i = 1:length(reactdata(:,1))
    fprintf(fid,'%s;\t',reactdata{i,1});
end
fprintf(fid,'\n');

% Write number of mols
fprintf(fid,'# No of mols\n');
for i = 1:length(reactdata(:,2))
    fprintf(fid,'%s;\t',reactdata{i,2});
end
fprintf(fid,'\n');

% Write temperature
fprintf(fid,'# Temperature\n');
for i = 1:length(reactdata(:,3))
    fprintf(fid,'%s;\t',reactdata{i,3});
end
fprintf(fid,'\n\n');

% Write products
proddata = get(handles.productstable2,'Data');
fprintf(fid,'%d\t # Products\n',length(proddata(:,1)));
for i = 1:length(proddata(:,1))
    fprintf(fid,'%s;\t',proddata{i,1});
end
fprintf(fid,'\n');

end

function SaveIsentropic(fid,handles)

fprintf(fid,'isentropic\t #Current running case\n\n');

% Write pressure
fprintf(fid,'%s; %d;\t # Reaction pressure\n\n',...
    get(handles.champressure3,'String'),...
    get(handles.pressureunits3,'Value'));

% Write flow type
if get(handles.equil,'Value')
    fprintf(fid,'shifting;\t #Flow type\n\n');
elseif get(handles.frozen,'Value')
    fprintf(fid,'frozen;\t #Flow type\n\n');
end

% Write outside pressure
if get(handles.ambpressure,'Value')
    fprintf(fid,'amb;\t #Outside pressure\n\n');
elseif get(handles.custpressure,'Value')
    fprintf(fid,'%s; %d;\t #Outside pressure\n\n',...
        get(handles.outpressure,'String'),...
        get(handles.outunits,'Value'));
end

% Write reactives
reactdata = get(handles.speciestable3,'Data');
fprintf(fid,'%d\t # Reactives\n',length(reactdata(:,1)));
for i = 1:length(reactdata(:,1))
    fprintf(fid,'%s;\t',reactdata{i,1});
end
fprintf(fid,'\n');

% Write number of mols
fprintf(fid,'# No of mols\n');
for i = 1:length(reactdata(:,2))
    fprintf(fid,'%s;\t',reactdata{i,2});
end
fprintf(fid,'\n');

% Write temperature
fprintf(fid,'# Temperature\n');
for i = 1:length(reactdata(:,3))
    fprintf(fid,'%s;\t',reactdata{i,3});
end

end