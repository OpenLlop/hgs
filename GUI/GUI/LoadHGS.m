function LoadHGS(file,handles)
%***********************************************************************************************************
%* HGS GRAPHIC v1.0 
%* By Lluis Amat Olondriz and Xavier Rodado Sanchez
%* With the collaboration of Manel Soria and Arnau Miro
%*
%* ETSEIAT UPC          
%***********************************************************************************************************

% Open file to read
fid = fopen(file);

% Read first case
Data = textscan(fid,'%s',1,'CommentStyle','#');

% Select by the case type
switch Data{1}{1}
    case 'equilibrium'
        LoadEquilibrium(fid,handles);
    case 'isenthalpic'
        LoadIsenthalpic(fid,handles);
    case 'isentropic'
        LoadIsentropic(fid,handles);
    otherwise
        errordlg('Cannot load! File is corrupt','Error in load');
end

% Close file
fclose(fid);

end

function LoadEquilibrium(fid,handles)

% Read pressure
Data = textscan(fid,'%s %d',1,'CommentStyle','#','Delimiter',';');
set(handles.champressure,'String',Data{1}{1});
set(handles.pressureunits,'Value',Data{2});

% Read reactives, mols and temperature
Data = textscan(fid,'%d',1,'CommentStyle','#','Delimiter',';'); len = Data{1};
Data = textscan(fid,'%s',3*len,'CommentStyle','#','Delimiter',';');
TableData = cell(len,4);
TableData(1:len,1) = Data{1}(1:len);
TableData(1:len,2) = Data{1}(len+1:2*len);
TableData(1:len,3) = Data{1}(2*len+1:3*len);
TableData(1:len,4) = {'K'};
set(handles.speciestable,'Data',TableData);

% Read products
Data = textscan(fid,'%d',1,'CommentStyle','#','Delimiter',';'); len = Data{1};
Data = textscan(fid,'%s',len,'CommentStyle','#','Delimiter',';');
set(handles.productstable,'Data',Data{1});

% Go to equilibrium
set(handles.toolbar_menu.Children(4),'state','on');
set(handles.toolbar_menu.Children(3),'state','off');
set(handles.toolbar_menu.Children(2),'state','off');
set(handles.equilibriumpanel,'visible','on');
set(handles.isenthalpicpanel,'visible','off');
set(handles.isentropicpanel,'visible','off');
set(handles.title,'String','HGS. Equilibrium');

end

function LoadIsenthalpic(fid,handles)

% Read pressure
Data = textscan(fid,'%s %d',1,'CommentStyle','#','Delimiter',';');
set(handles.champressure2,'String',Data{1}{1});
set(handles.pressureunits2,'Value',Data{2});

% Read reactives, mols and temperature
Data = textscan(fid,'%d',1,'CommentStyle','#','Delimiter',';'); len = Data{1};
Data = textscan(fid,'%s',3*len,'CommentStyle','#','Delimiter',';');
TableData = cell(len,4);
TableData(1:len,1) = Data{1}(1:len);
TableData(1:len,2) = Data{1}(len+1:2*len);
TableData(1:len,3) = Data{1}(2*len+1:3*len);
TableData(1:len,4) = {'K'};
set(handles.speciestable2,'Data',TableData);

% Read products
Data = textscan(fid,'%d',1,'CommentStyle','#','Delimiter',';'); len = Data{1};
Data = textscan(fid,'%s',len,'CommentStyle','#','Delimiter',';');
set(handles.productstable2,'Data',Data{1});

% Go to isenthalpic
set(handles.toolbar_menu.Children(4),'state','off');
set(handles.toolbar_menu.Children(3),'state','on');
set(handles.toolbar_menu.Children(2),'state','off');
set(handles.equilibriumpanel,'visible','off');
set(handles.isenthalpicpanel,'visible','on');
set(handles.isentropicpanel,'visible','off');
set(handles.title,'String','HGS. Adiabatic Flame Temperature');

end

function LoadIsentropic(fid,handles)

% Read pressure
Data = textscan(fid,'%s %d',1,'CommentStyle','#','Delimiter',';');
set(handles.champressure3,'String',Data{1}{1});
set(handles.pressureunits3,'Value',Data{2});

% Read flow type
Data = textscan(fid,'%s',1,'CommentStyle','#','Delimiter',';');
switch Data{1}{1}
    case 'shifting'
        set(handles.equil,'Value',1);
        set(handles.frozen,'Value',0);
    case 'frozen'
        set(handles.equil,'Value',0);
        set(handles.frozen,'Value',1);
    otherwise
        errordlg('Cannot load! File is corrupt','Error in load');       
end

% Read outside pressure
Data = textscan(fid,'%s',1,'CommentStyle','#','Delimiter',';');
if strcmp(Data{1}{1},'amb')
    set(handles.ambpressure,'Value',1);
    set(handles.custpressure,'Value',0);
    set(handles.outpressure,'Enable','off');
    set(handles.outunits,'Enable','off');
else
    set(handles.ambpressure,'Value',0);
    set(handles.custpressure,'Value',1);
    set(handles.outpressure,'Enable','on');
    set(handles.outunits,'Enable','on');
    set(handles.outpressure,'String',Data{1}{1});
    Data = textscan(fid,'%d',1,'CommentStyle','#','Delimiter',';');
    set(handles.outunits,'Value',Data{1});
end

% Read reactives, mols and temperature
Data = textscan(fid,'%d',1,'CommentStyle','#','Delimiter',';'); len = Data{1};
Data = textscan(fid,'%s',3*len,'CommentStyle','#','Delimiter',';');
TableData = cell(len,4);
TableData(1:len,1) = Data{1}(1:len);
TableData(1:len,2) = Data{1}(len+1:2*len);
TableData(1:len,3) = Data{1}(2*len+1:3*len);
TableData(1:len,4) = {'K'};
set(handles.speciestable3,'Data',TableData);

% Go to isentropic
set(handles.toolbar_menu.Children(4),'state','off');
set(handles.toolbar_menu.Children(3),'state','off');
set(handles.toolbar_menu.Children(2),'state','on');
set(handles.equilibriumpanel,'visible','off');
set(handles.isenthalpicpanel,'visible','off');
set(handles.isentropicpanel,'visible','on');
set(handles.title,'String','HGS. Isentropic Expansion');

end