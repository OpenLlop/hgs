function CleanHGS(handles)
%***********************************************************************************************************
%* HGS GRAPHIC v1.0 
%* By Lluis Amat Olondriz and Xavier Rodado Sanchez
%* With the collaboration of Manel Soria and Arnau Miro
%*
%* ETSEIAT UPC          
%***********************************************************************************************************

% Clean chemical equilibrium 
set(handles.speciestable,'Data',[]);
set(handles.productstable,'Data',[]);
set(handles.outputtable,'Data',[]);
set(handles.champressure,'String',' ');
set(handles.pressureunits,'Value',1);

% Clean adiabatic flame temperature
set(handles.speciestable2,'Data',[]);
set(handles.productstable2,'Data',[]);
set(handles.outputtable2,'Data',[]);
set(handles.champressure2,'String',' ');
set(handles.pressureunits2,'Value',1);
set(handles.equilTemp,'String',' ');

%Clean isentropic
set(handles.speciestable3,'Data',[]);
set(handles.outputtable3,'Data',[]);
set(handles.performancetable,'Data',[]);
set(handles.champressure3,'String',' ');
set(handles.pressureunits3,'Value',1);
set(handles.equil,'Value',1);
set(handles.ambpressure,'Value',1);
set(handles.outpressure,'String',' ');
set(handles.outunits,'Value',1);
set(handles.outpressure,'Enable','off')
set(handles.outunits,'Enable','off')
end