function species_list=filterBurcat
%***********************************************************************************************************
%* HGS GRAPHIC v1.0 
%* By Lluis Amat Olondriz and Xavier Rodado Sanchez
%* With the collaboration of Manel Soria and Arnau Miro
%*
%* ETSEIAT UPC          
%***********************************************************************************************************

% loads all the names of the species from BurcatDB into the list of species
% string
global BurcatDB;

if (~exist('BurcatDB','var') || isempty(BurcatDB)), load BurcatDB; end

a=1;
for i=1:length(BurcatDB)
    % filters the species whose elements are: H,N,C,O,He 
    if all(ismember(BurcatDB{i}.Element,'0123456789HNOCe'))==1
        species_list{a}=BurcatDB{i}.Element;
        a=a+1;
    end
end

end