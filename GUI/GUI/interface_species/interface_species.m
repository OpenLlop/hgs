function varargout = interface_species(varargin)
%***********************************************************************************************************
%* HGS GRAPHIC v1.0 
%* By Lluis Amat Olondriz and Xavier Rodado Sanchez
%* With the collaboration of Manel Soria and Arnau Miro
%*
%* ETSEIAT UPC          
%***********************************************************************************************************

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @interface_species_OpeningFcn, ...
                   'gui_OutputFcn',  @interface_species_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before interface_species is made visible.
function interface_species_OpeningFcn(hObject, eventdata, handles, varargin)

% loads the handles from interface_main so they can be accessed in this
% figure
handles.main = guidata(MainWindow('iconEditor', hObject));

% Choose default command line output for interface_species
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);


% UIWAIT makes interface_species wait for user response (see UIRESUME)
% uiwait(handles.species_selector);


% --- Outputs from this function are returned to the command line.
function varargout = interface_species_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;




% --- Executes during object creation, after setting all properties.
function species_list_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% loads all the names of the species filtered from BurcatDB into the list of species
% string
species_list=filterBurcat;

set(hObject,'String',species_list);


% --- Executes during object creation, after setting all properties.
function species_table_CreateFcn(hObject, eventdata, handles)

global TableSelect

% obtention of the current elements in the table of the main window
handles.main=guidata(MainWindow);

% Select table to load
if TableSelect==0, Table_elements=get(handles.main.speciestable,'Data'); end
if TableSelect==1, Table_elements=get(handles.main.productstable,'Data'); end
if TableSelect==2, Table_elements=get(handles.main.speciestable2,'Data'); end
if TableSelect==3, Table_elements=get(handles.main.productstable2,'Data'); end
if TableSelect==4, Table_elements=get(handles.main.speciestable3,'Data'); end

handles.Elements=Table_elements(:,1);
set(hObject,'Data',handles.Elements);


% --- Executes on button press in select_specie.
function select_specie_Callback(hObject, eventdata, handles)

% obtention of the element selected on the list
i=get(handles.species_list,'Value');
Elements1=get(handles.species_list,'String');

% obtention of the current data from the table of species
Table_elements=get(handles.species_table,'Data');

% addition of the element selected to Table_elements in a new row and
% default value of 0 mols of initial quantity
if size(Table_elements,1)==0
    Table_elements(end+1,1)=Elements1(i);
else
    % verification that the element is not already present in
    % Table_elements
    k=0;
    for j=1:size(Table_elements,1)
        k=k+strcmp(Table_elements(j,1),Elements1(i));        
    end
    if k==0
        Table_elements(end+1,1)=Elements1(i);        
    end
end

% update the table of species to the current data with the new element
% selected
set(handles.species_table,'Data',Table_elements);

% --- Executes on button press in delete_specie.
function delete_specie_Callback(hObject, eventdata, handles)

% getting the position of the selected element in the table that is going
% to be deleted
i=get(handles.species_table,'UserData');

% deletes the row from the table of the selected element
if numel(i)~=0
    Table.elements=get(handles.species_table,'Data');
    if size(Table.elements,1)~=0
        Table.elements(i(1),:)=[];
        set(handles.species_table,'Data',Table.elements);
    end
end


% --- Executes on button press in clear_all.
function clear_all_Callback(hObject, eventdata, handles)

% gets the current data of the table of species
Table.elements=get(handles.species_table,'Data');

% clears all the rows
for j=1:size(Table.elements,1)
    Table.elements(end,:)=[];
end

% updates the data with the current cell values
set(handles.species_table,'Data',Table.elements);

function species_searcher_Callback(hObject, eventdata, handles)

deb=1;
set(handles.species_list,'Value',deb);

% loads all the names of the species filtered from BurcatDB into the list of species
% string
Elements=filterBurcat;

% gets the element introduced by the user
Search_term=get(hObject,'String');

% setting the cell that will contain the search results
Elements2={};

% search BurcatDB species list for matchings with the search term
if numel(Search_term)~=0
    for j=1:size(Elements,2)
        Matchings=strncmp(Elements(j),Search_term,length(Search_term));
        if Matchings~=0
            Elements2(end+1)=Elements(j);
        end
    end
else
    % if the searching bar is left blank, it returns all the initial list
    % of species
    Elements2=Elements;
end


% update the list of species to the current data with the new element
% selected
set(handles.species_list,'String',Elements2);



% --- Executes during object creation, after setting all properties.
function species_searcher_CreateFcn(hObject, eventdata, handles)
% hObject    handle to species_searcher (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes when selected cell(s) is changed in species_table.
function species_table_CellSelectionCallback(hObject, eventdata, handles)
% saves the user selected into UserData
set(handles.species_table,'UserData',eventdata.Indices);



% --- Executes on button press in continue1.
function continue1_Callback(hObject, eventdata, handles)
global TableSelect

% obtention of the current data from the table of species
Elements1=get(handles.species_table,'Data');

% Create table
Table_elements = cell(length(Elements1),4);
Table_elements(:,1)=Elements1;

% Select table
if TableSelect==0, set(handles.main.speciestable,'Data',Table_elements); end
if TableSelect==1, set(handles.main.productstable,'Data',Table_elements);end
if TableSelect==2, set(handles.main.speciestable2,'Data',Table_elements); end
if TableSelect==3, set(handles.main.productstable2,'Data',Table_elements);end
if TableSelect==4, set(handles.main.speciestable3,'Data',Table_elements); end

% closes the window
delete(handles.species_selector);



% --- Executes on button press in cancel1.
function cancel1_Callback(hObject, eventdata, handles)

% closes the window without saving any changes
delete(handles.species_selector);


% --- Executes when user attempts to close species_selector.
function species_selector_CloseRequestFcn(hObject, eventdata, handles)

delete(hObject);


% --- Executes on selection change in species_list.
function species_list_Callback(hObject, eventdata, handles)
% hObject    handle to species_list (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
