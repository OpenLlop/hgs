function [Isp,cstar]=RocketPerformance(Ve,gamma,R,MM,Tc)
%***********************************************************************************************************
%* HGS GRAPHIC v1.0 
%* By Arnau Miro
%*
%* ETSEIAT UPC          
%***********************************************************************************************************

Isp=Ve/9.81;
cstar=sqrt(gamma*((R*Tc)/MM))/(gamma*(2/(gamma+1))^((gamma+1)/(2*(gamma-1))));

end